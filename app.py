from pathlib import Path
import base64
import streamlit as st
from PIL import Image
from datetime import datetime
import streamlit.components.v1 as components


def get_years_diff():
    desired_date = datetime(2019, 3, 14).date()
    current_date = datetime.now().date()
    date_difference = current_date - desired_date
    years_difference = date_difference.days / 365
    return int(years_difference)

# --- PATH SETTINGS ---
current_dir = Path(__file__).parent if "__file__" in locals() else Path.cwd()
css_file = current_dir / "styles" / "main.css"
resume_file = current_dir / "assets" / "Burlutsky_Oleksandr_Resume.pdf"
profile_pic = current_dir / "assets" / "profile-pic.png"
blured_cover_pic = current_dir / "assets" / "cover-image.jpg"


# --- GENERAL SETTINGS ---
PAGE_TITLE = "Digital CV | Oleksandr Burlutsky"
PAGE_ICON = ":wave:"
NAME = "Oleksandr Burlutsky"
DESCRIPTION = """Software Quality Assurance Engineer"""
EMAIL = "burlutskyialeksandr@gmail.com"
SOCIAL_MEDIA = {
    "GitHub": "https://github.com",
    "LinkedIn": "https://www.linkedin.com/in/aleksandr-burlutskyi/",
    "Skype": "join.skype.com/invite/ophFIhLH0sDf",
    "Telegram": "https://t.me/ABurll"
}
LANGUAGES = """
              🇺🇸 English : intermediate level\n
              🇺🇦 Ukrainian : native level
            """

WORK_HISTORY = f"""
**AQA | eMotion**\n
*03/2019 - Till Now*\n
- ► As Automation QA:
Development of automated tests deployed on a remote server using Python, Pytest, Selenium, Allure, Jenkins.
Load testing with Jmeter and Locust.
Development of automated tests for API with Javascript, Postman and Newman
- ► As Manual QA:
Defect tracking, Test Design, Requirements Analysis. Exploratory, Black Box, Smoke, Regression, Sanity, Integration, 
Functional and Non-Functional testing of the API, UI, backend and DB side
------------------------------------------------------------------------------------------------------------------
"""

HARD_SKILLS = """
- 🛠 Programming: Python, Pytest, Selenium Webdriver/Grid, Allure, Locust, SQL, JavaScript
- 📚 Modeling: Logistic regression, linear regression
- 🗄️ Databases: Postgres, SQL Server, Redis
"""

QUALIFICATION = f"""
- ✔️ Over {get_years_diff()} years experience with manual and automation testing
- ✔️ Strong hands on experience in developing automation project from scratch
- ✔️ Experience in API, UI, Black/White Box, testing. 
- ✔️ Planning, development and execution of load tests on API and UI side;
- ✔️ Good understanding of Agile principles and testing methodologies
- ✔️ Excellent team-player and displaying strong sense of initiative on tasks
"""


def expand_me(label: str, data: str):
    st.write('\n')
    my_expander = st.expander(label=label)
    with my_expander:
        st.write(data)


def add_bg_from_local(image_file):
    with open(image_file, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    st.markdown(
    f"""
    <style>
    .stApp {{
        background-position: center;
        background-image: url(data:image/{"jpg"};base64,{encoded_string.decode()});
        background-size: cover;
    }}
    </style>
    """,
        unsafe_allow_html=True
    )


st.set_page_config(page_title=PAGE_TITLE, page_icon=PAGE_ICON)

# --- LOAD CSS, PDF & PROFIL PIC ---
with open(css_file) as f:
    st.markdown("<style>{}</style>".format(f.read()), unsafe_allow_html=True)
with open(resume_file, "rb") as pdf_file:
    PDFbyte = pdf_file.read()
profile_pic = Image.open(profile_pic)
add_bg_from_local(blured_cover_pic)

# --- HERO SECTION ---
col1, col2 = st.columns(2, gap="small")

with col1:
    st.image(profile_pic, width=230)

with col2:
    st.title(NAME)
    st.write(DESCRIPTION)
    st.download_button(
        label=" 📄 Download Resume",
        data=PDFbyte,
        file_name=resume_file.name,
        mime="application/octet-stream",
    )
    st.write("📩", EMAIL)


# --- SOCIAL LINKS ---
st.write('\n')
cols = st.columns(len(SOCIAL_MEDIA))
for index, (platform, link) in enumerate(SOCIAL_MEDIA.items()):
    cols[index].write(f"[{platform}]({link})")

expand_me("Experience & Qualifications", QUALIFICATION)
expand_me("Hard Skills", HARD_SKILLS)
expand_me("Work History", WORK_HISTORY)
expand_me("Languages", LANGUAGES)


